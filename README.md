## JS-06 - React example project - Http requests
This is an example React project covering multiple React features. Based on UDEMY course: https://www.udemy.com/react-the-complete-guide-incl-redux/.

**Examples in this project are focused mainly on HTTP requests and routing.**

#### Used commands and libs:
* create-react-app my-app-name
* npm install
* npm start
* npm install --save radium
* npm run eject
* npm install --save prop-types
* npm install axios --save
* npm install --save react-router react-router-dom

