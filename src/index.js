import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import axios from 'axios';

axios.defaults.baseURL = 'https://jsonplaceholder.typicode.com';
axios.defaults.headers.common['Authorization'] = 'AUTH_TOKEN';
axios.defaults.headers.post['Content-Type'] = 'application/json';

// these interceptors will be shared all over the places in the project where we use axios
// can be used for example for authorization, where we add authorization header to the request
axios.interceptors.request.use(request => {
    console.log(request);
    // Edit request config
    return request; // we have to return else we block the request
}, error => {   // this error is not handling errors from server (e.g. 400, ...), but errors like no internet connection, server unreachable, ...
    console.log(error);
    return Promise.reject(error);   // for forwarding to request where it was created - to process in CATCH promise
});

// same rules as in the section above, but this is used for processing the server response (not the request)
axios.interceptors.response.use(response => {
    console.log(response);
    return response
}, error => {
    console.log(error);
    return Promise.reject(error);
});

/*
Example how to remove interceptors (https://github.com/axios/axios#interceptors)
var myInterceptor = axios.interceptors.request.use(function () {...});
axios.interceptors.request.eject(myInterceptor);
*/

ReactDOM.render( <App />, document.getElementById( 'root' ) );
registerServiceWorker();
