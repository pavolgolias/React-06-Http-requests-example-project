import React, { Component } from 'react';
import {Route, NavLink, Switch, Redirect} from 'react-router-dom';

import './Blog.css';
import Posts from './Posts/Posts';
//import NewPost from './NewPost/NewPost';
import asyncComponent from '../../hoc/asyncComponent';

const AsyncNewPost = asyncComponent(() => {
    return import('./NewPost/NewPost');    // dynamic import syntax
});

class Blog extends Component {
    state= {
        auth: true
    };

    render () {
        return (
            <div className="Blog">
                <header>
                    <nav>
                        <ul>
                            <li>
                                <NavLink
                                to="/posts"
                                exact
                                activeClassName="my-active"
                                activeStyle={{
                                    color: '#fa923f',
                                    textDecoration: 'underline'
                                }}>
                                    Posts
                                 </NavLink>
                            </li>
                            <li>
                                <NavLink to={{
                                pathname: '/new-post',
                                hash: '#submit',
                                search: '?quick-submit=true'
                                }}>
                                    New post
                                </NavLink>
                            </li>
                        </ul>
                    </nav>
                </header>

                {/*
                Order of <Route /> elements is important!
                path == prefix // exact == exact like path // render == what to render
                <Route path="/" render={() => <h1>HOME</h1>} />
                <Route path="/2" exact render={() => <h1>HOME 2</h1>} />*/}

                <Switch>
                    {this.state.auth ? <Route path="/new-post" component={AsyncNewPost} /> : null }
                    <Route path="/posts" component={Posts} />
                    <Route render={() => <h1>Not found</h1>}/>
                    {/*<Redirect from="/" to="/posts" />*/}
                    {/*<Route path="/" component={Posts} />*/}
                    {/*<Route path="/:id" exact component={FullPost} />*/}
                </Switch>

            </div>
        );
    }
}

export default Blog;

// Absolute vs Relative path
// Link to is always absolute (URL starts from domain) - if we want to have relative path, we have to build it dynamically (e.g. from match object)
